import React from "react";
import PropTypes from "prop-types";

import MessageList from "./MessageList";

function MessageListContainer({ messages, toggleUpdateMessageForm }) {
  return (
    <MessageList
      messages={messages}
      toggleUpdateMessageForm={toggleUpdateMessageForm}
    />
  );
}

MessageListContainer.propTypes = {
  messages: PropTypes.array.isRequired,
};

MessageListContainer.defaultProps = {
  messages: [],
};

export default MessageListContainer;
