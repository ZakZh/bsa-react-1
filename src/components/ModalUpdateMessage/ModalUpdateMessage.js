import React from "react";
import { Button, Header, Image, Modal, TextArea } from "semantic-ui-react";
import PropTypes from "prop-types";

function ModalUpdateMessage({
  toggleUpdateMessageForm,
  updatedMessage,
  setUpdateMessage,
  updateMessage,
}) {
  return (
    <Modal
      dimmer="blurring"
      centered={true}
      open
      onClose={() => toggleUpdateMessageForm()}
      style={{
        height: "auto",
        top: "auto",
        left: "auto",
        bottom: "auto",
        right: "auto",
      }}
    >
      <Modal.Header className="modal-header">Update message</Modal.Header>
      <Modal.Content className="">
        <TextArea
          className=""
          id="newMessageTextArea"
          style={{ width: "100%" }}
          value={updatedMessage.text}
          onChange={(e) => {
            setUpdateMessage(e.target.value);
          }}
          rows="5"
        />
      </Modal.Content>
      <Modal.Actions className="">
        <Button
          content="Close"
          className=""
          onClick={() => toggleUpdateMessageForm()}
        />
        <Button
          content="Update message"
          className=""
          onClick={updateMessage}
          primary
        />
      </Modal.Actions>
    </Modal>
  );
}

export default ModalUpdateMessage;
