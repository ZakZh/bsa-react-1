import React from "react";
import PropTypes from "prop-types";

import MessageInput from "./MessageInput";

function MessageInputContainer({ sendMessage, setNewMessageValue, message }) {
  return (
    <MessageInput
      sendMessage={sendMessage}
      setNewMessageValue={setNewMessageValue}
      message={message}
    />
  );
}

MessageInputContainer.propTypes = {
  sendMessage: PropTypes.func.isRequired,
  setNewMessageValue: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired,
};

export default MessageInputContainer;
