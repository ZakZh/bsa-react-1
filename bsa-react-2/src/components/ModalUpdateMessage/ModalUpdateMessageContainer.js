import React from "react";
import ModalUpdateMessage from "./ModalUpdateMessage";
import PropTypes from "prop-types";

function ModalUpdateMessageContainer({
  toggleUpdateMessageForm,
  updatedMessage,
  setUpdateMessage,
  updateMessage,
}) {
  return (
    <ModalUpdateMessage
      toggleUpdateMessageForm={toggleUpdateMessageForm}
      updatedMessage={updatedMessage}
      setUpdateMessage={setUpdateMessage}
      updateMessage={updateMessage}
    />
  );
}

export default ModalUpdateMessageContainer;
